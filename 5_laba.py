# Обращаемся к библиотеке
import math

while True:
    mass_f, mass_x = [], []
    xmin, xmax, a, x = 0, 0, 0, 0
    while True:
        try:  # Выбор уравнения
            function = int(input('Выберите уравнение G(1), F(2), Y(3):'))
            if function < 1 or function > 3:
                print('Нет такого уравнения!')
            else:
                break
        except ValueError:
            print('Ошибка: неправильный формат')

    # Шаблон чисел

    while True:
        try:  # Ввод переменных
            a = float(input('Введите значение a:'))
            xmin = int(input("Введите мин значение x:"))
            xmax = int(input("Введите макс значение x:"))
            if xmin >= xmax:
                print('Неправильно введеный границы х')
            else:
                break
        except ValueError:
            print('Ошибка: неправильный формат')

    while True:  # Количество шагов
        try:
            steps = int(input('Кол-во шагов: '))
            if math.isclose(steps, 0.0) or steps < 0 or (xmax - xmin) < steps:
                print('Ошибка: введите еще раз')
            else:
                size = (xmax - xmin) / steps
                break
        except ValueError:
            print('Ошибка: неправильный формат')

    x, step, i = xmin, 0, 1
    string = ''

    if function == 1:
        # Решение уравнения G и его вывод
        while step <= steps:
            g1 = 6 * (4 * a ** 2 - 12 * a * x + 5 * x ** 2)
            g2 = 9 * a ** 2 + 30 * a * x + 16 * x ** 2
            if math.isclose(g2, 0.0):
                mass_f.append(None)
                mass_x.append(str(x))
                print('Нет решений, ошибка: знаменатель = 0')
            else:
                g = g1 / g2
                string += str(g)
                mass_f.append(g)
                mass_x.append(str(x))
                print(i, ': x = {:.5f}'.format(x), ' || G = {:.5f}'.format(g))
            x += size
            step += 1
            i += 1
        print('min G:', min(mass_f, key=lambda i: str(i), default='None'),
              '\nmax G:', max(mass_f, key=lambda i: str(i), default='None'))
        print('Строка значений G:', string)

    elif function == 2:
        # Решение уравнения F и его вывод
        while step <= steps:
            f = math.sin(28 * a ** 2 - 27 * a * x + 5 * x ** 2)
            if f > 1 or f < -1:
                mass_f.append(None)
                mass_x.append(str(x))
                print('Нет решений, ошибка: не принадлежит области значений функции')
            else:
                f = math.sin(28 * a ** 2 - 27 * a * x + 5 * x ** 2)
                string += str(f)
                mass_f.append(f)
                mass_x.append(str(x))
                print(i, ': x = {:.5f}'.format(x), '|| F = {:.5f}'.format(f))
            x += size
            step += 1
            i += 1
        print('min F:', min(mass_f, key=lambda i: str(i), default='None'),
              '\nmax F:', max(mass_f, key=lambda i: str(i), default='None'))
        print('Строка значений F:', string)

    elif function == 3:
        # Решение уравнения Y и его вывод
        while step <= steps:
            y1 = (21 * a ** 2 + 73 * a * x + 10 * x ** 2 + 1)
            if y1 <= 0:
                mass_f.append(None)
                mass_x.append(str(x))
                print('Нет решений, ошибка: не принадлежит области допустимых значений функции')
            else:
                y = (math.log(y1)) / (math.log(2))
                string += str(y)
                mass_f.append(y)
                mass_x.append(str(x))
                print(i, ': x = {:.5f}'.format(x), '|| Y = {:.5f}'.format(y))
            x += size
            step += 1
            i += 1
        print('min Y:', min(mass_f, key=lambda i: str(i), default='None'),
              '\nmax Y:', max(mass_f, key=lambda i: str(i), default='None'))
        print('Строка значений Y:', string)

    while True:
        try:
            num = str(input('Что искать?: '))
            print('Количество совпадений: ', string.count(num))
            break
        except ValueError:
            print('Цифры пиши')

    # Выход из цикла по требованию пользователя
    while True:
        try:
            end = int(input("Выйти из цикла? ДА[1]/НЕТ[2]: "))
            if end == 1:
                print('Выхожу из программы...')
                exit(0)
            elif end == 2:
                print("Заново производим цикл")
                break
            else:
                print("Ошибка: нет такого варианта выбора, выберите 0 или 1")
        except ValueError:
            print("Ошибка: неправильный формат")
